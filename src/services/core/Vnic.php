<?php

namespace yanlongli\OCI\services\core;

use yanlongli\OCI\Exception;
use yanlongli\OCI\service\Resource;
use yanlongli\OCI\services\core\vnic\responses\GetVnic;

class Vnic extends Resource
{
    /**
     * @param $vnicId
     * @return GetVnic
     * @throws Exception
     */
    public function GetVnic($vnicId)
    {
        return new GetVnic($this->call(__METHOD__, [
            'httpPath' => '/20160918/vnics/{vnicId}',
            'pathParams' => [
                'vnicId' => $vnicId,
            ]
        ]));
    }
}
