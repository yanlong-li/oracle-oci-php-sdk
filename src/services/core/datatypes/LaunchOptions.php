<?php

namespace yanlongli\OCI\services\core\datatypes;

use yanlongli\OCI\contract\DataTypes;

class LaunchOptions extends DataTypes
{
    private $bootVolumeType;
    private $firmware;
    private $isConsistentVolumeNamingEnabled;
    private $isPvEncryptionInTransitEnabled;
    private $networkType;
    private $remoteDataVolumeType;
}
