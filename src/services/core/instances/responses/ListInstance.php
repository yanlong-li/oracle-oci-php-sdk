<?php

namespace yanlongli\OCI\services\core\instances\responses;

use yanlongli\OCI\contract\Response;
use yanlongli\OCI\services\core\datatypes\Instance;

class ListInstance extends Response
{
    /**
     * @return Instance[]
     */
    public function getList()
    {
        $content = $this->getContents();
        $arr = json_decode($content, true);
        return Instance::fromIndexArray($arr);
    }
}
